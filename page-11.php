<?php get_header()?>

<div class="hero-full">
  <div class="ks-title-wrap">
    <h1 class="ks-title">Mi stvaramo čarape za celu porodicu sa ljubavlju</h1>
    <a href="http://localhost/kolysocks/kategorija-proizvoda/bebe/" class="ks-btn-action u-m10 nm_btn nm_btn nm_btn_md nm_btn_filled_rounded">Nova kolekcija</a>
  </div>
</div>

<section class="ks-products">
  <div class="ks-wrap">
    <h2 class="ks-section-title-big">Naše čarapice</h2>
    <p class="ks-desc">Lorem ipsum sit dolarem amet.</p>
  </div>

  <!-- row 1-->
  <div class="nm-row">
    <div class="col-md-4 ks-products-col">
      <img src="http://localhost/kolysocks/wp-content/uploads/2016/10/novorodnjeni.jpg" alt="" width="450" height="450">
      <!-- <div class="ks-mask"></div> -->
      <!-- <h3 class="ks-products-title">Novorođenčad</h3> -->
      <a href="" class="ks-btn-action nm_btn nm_btn nm_btn_md nm_btn_filled_rounded"><h3>Novorođenčad</h3></a>

    </div>

    <div class="col-md-4 ks-products-col">
      <img src="http://localhost/kolysocks/wp-content/uploads/2016/10/bebe.jpg" alt="" width="450" height="450">
      <!-- <div class="ks-mask"></div> -->
      <!-- <a class="ks-products-btn">Test</a> -->
      <!-- <h3 class="ks-products-title">Bebe</h3> -->
      <a href="" class="ks-btn-action nm_btn nm_btn nm_btn_md nm_btn_filled_rounded"><h3>Bebe</h3></a>

    </div>

    <div class="col-md-4 ks-products-col">
      <img src="http://localhost/kolysocks/wp-content/uploads/2016/10/deca.jpg" alt="" width="450" height="450">
      <!-- <div class="ks-mask"></div> -->
      <!-- <h3 class="ks-products-title">Deca</h3> -->
      <a href="" class="ks-btn-action nm_btn nm_btn nm_btn_md nm_btn_filled_rounded"><h3>Deca</h3></a>

      <!-- <a class="ks-products-btn">Test</a> -->
    </div>

  </div>
  <!-- END OF 1 ROW-->

  <!-- row 2 -->
  <div class="nm-row">
    <div class="col-md-4 ks-products-col">
      <img src="http://localhost/kolysocks/wp-content/uploads/2016/10/muskarci.jpg" alt="" width="450" height="450">
      <!-- <div class="ks-mask"></div>
      <a class="ks-products-btn">Test</a> -->
      <!-- <h3 class="ks-products-title"> Muškarci</h3> -->
      <a href="" class="ks-btn-action nm_btn nm_btn nm_btn_md nm_btn_filled_rounded"><h3>Muškarci</h3></a>
    </div>

    <div class="col-md-4 ks-products-col">
      <img src="http://localhost/kolysocks/wp-content/uploads/2016/10/zene.jpg" alt="" width="450" height="450">
      <h3></h3>
      <!-- <div class="ks-mask"></div>
      <a class="ks-products-btn">Test</a> -->

      <!-- <h3 class="ks-products-title">Žene</h3> -->
      <a href="" class="ks-btn-action nm_btn nm_btn nm_btn_md nm_btn_filled_rounded"><h3>Žene</h3></a>
    </div>

    <div class="col-md-4 ks-products-col">
      <img src="http://localhost/kolysocks/wp-content/uploads/2016/10/svecano.jpg" alt="" width="450" height="450">
      <!-- <div class="ks-mask"></div>
      <a class="ks-products-btn">Test</a> -->
      <!-- <h3 class="ks-products-title">Svečano</h3> -->
      <a href="" class="ks-btn-action nm_btn nm_btn nm_btn_md nm_btn_filled_rounded"><h3>Svečano</h3></a>
    </div>
  </div>
  <!-- END OF ROW 2-->
</section>
<div class="line32"></div>

<?php get_footer(); ?>